package com.softserve.edu.zhyliaiev.test;

import com.softserve.edu.zhyliaiev.comments.Comment;
import com.softserve.edu.zhyliaiev.pages.EditorPage;
import com.softserve.edu.zhyliaiev.pages.IndexPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.*;

public class PageObjectTests {
    private WebDriver driver;
    private String baseURL;

    @BeforeTest
    public void setUp() {
        driver = new FirefoxDriver();
        baseURL = "http://commentssprintone.azurewebsites.net/";
    }

    @AfterTest
    public void tearDown() {
        try {
            driver.close();
        } catch (Exception e) {
            // ignore;
        }
    }

    @Test

    public void categoriesTest() {
        driver.get(baseURL + "/Editor/NewComment");
        EditorPage currentPage = new EditorPage(driver);

        Comment comment = new Comment("777", "HelloTest", true, Arrays.asList("Cat0", "Cat1"));

        currentPage.setCommentData(comment);

        comment = new Comment("777", "HelloTest", true, Arrays.asList("Cat3", "Cat2"));

        currentPage.setCommentData(comment);

        System.out.println(currentPage.getCommentTextField().getAttribute("value"));


    }


    @Test
    public void pageObjectTest() {
        driver.get(baseURL);
        IndexPage indexPage = new IndexPage(driver);

        EditorPage duplicatePage = indexPage.duplicateComment(3);

        String initialText = duplicatePage.getCommentTextField().getAttribute("value");

        System.out.println(initialText);

        initialText = initialText.replace("Copy of ", "");
        duplicatePage.getCommentTextField().clear();
        duplicatePage.getCommentTextField().sendKeys(initialText);

        duplicatePage = duplicatePage.unsuccessfulSaveAndReturn();
        String expected = "Comment already exists";
        System.out.println(expected);
        String actual = duplicatePage.getGeneralValidationError().getText();
        System.out.println(actual);

        Comment comment = new Comment("777", "HelloTest", true, Arrays.asList("Cat0", "Cat1"));

        duplicatePage.setCommentData(comment);

        System.out.println(duplicatePage.getSelectedCategories().getCheckboxes());





        /*System.out.println(indexPage.getActionDropdown().getText());


        testTable(indexPage);

        indexPage = indexPage.getCommentsTable().goToPage("3");

        testTable(indexPage);

        indexPage = indexPage.getCommentsTable().goToPrevPage();

        testTable(indexPage);

        indexPage = indexPage.getCommentsTable().goToNextPage();

        testTable(indexPage);

        indexPage = indexPage.getCommentsTable().sortByNumber();

        testTable(indexPage);*/
    }

    @Test
    public void EditorPageTest() {
        driver.get("http://commentssprintone.azurewebsites.net/Editor/NewComment");
       /*  // get all checkboxes in chosen box
        List<WebElement> categories = driver
                .findElement(By.id("alvailablecategories"))
                .findElements(By.xpath(".//div/input[@type=\"checkbox\"]"));
        int i = 1;
        for (WebElement category : categories) {
            if (category.getAttribute("value").equals("2")) {
                category.click();
            } else {
            }
            System.out.println(category.getAttribute("name"));*/

        // get pairs checkbox-name

        HashMap<WebElement, String> checkboxes = new HashMap<WebElement, String>();
        List<WebElement> tmp = driver.findElement(
                By.id("alvailablecategories"))
                .findElements(
//                            By.xpath(".//div/input[@type=\"checkbox\"]"));
                        By.xpath(".//div[@class=\"categoryitem\"]"));
        for (WebElement webElement : tmp) {
            checkboxes.put(webElement.findElement(
                    By.xpath(".//input[@type=\"checkbox\"]")),
                    webElement.findElement(By.xpath(".//span")).getText());
        }

        for (Map.Entry<WebElement, String> entry : checkboxes.entrySet()) {
            System.out.println(entry.getKey().getAttribute("type") + " - " + entry.getValue());
        }



    }

    public void testTable(IndexPage indexPage) {
        System.out.println("Page " + indexPage.getCommentsTable().getCurrentPageNumber() + ":");
        System.out.println(indexPage.getCommentsTable().getCell(3, 3).getText());
        System.out.println(indexPage.getCommentsTable().getComment(5).toString());
        indexPage.getCommentsTable().checkOffComment(1);
        indexPage.getCommentsTable().checkOffComment(3);
        System.out.println("----------------------------------\n");
    }



}
