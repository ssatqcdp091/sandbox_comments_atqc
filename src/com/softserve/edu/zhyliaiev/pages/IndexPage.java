package com.softserve.edu.zhyliaiev.pages;

import com.softserve.edu.zhyliaiev.comments.Comment;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Arrays;
import java.util.List;

public class IndexPage {
    private WebDriver driver;
    private WebElement returnLink;
    private WebElement newLink;
    private WebElement duplicateButton;
    private WebElement editButton;
    private WebElement deleteButton;
    private WebElement actionDropdown;
    private WebElement applyButton;
    private WebElement categoryNameDropdown;
    private WebElement statusesDropdown;
    private CommentsTable commentsTable;
    private WebElement infoField;
    private Alert popup;


    public IndexPage(final WebDriver driver) {
        this.driver = driver;

        // Check Page Exist
        if (!driver.getTitle().toLowerCase().startsWith("index")) {
            throw new RuntimeException("This is not the index page");
        }

        setReturnLink();
        setNewLink();
        setDuplicateButton();
        setEditButton();
        setDeleteButton();
        setActionDropdown();
        setApplyButton();
        setCategoryNameDropdown();
        setStatusesDropdown();
        setCommentsTable();
        setInfoField();
    }

    // Getters/Setters section
    public WebElement getReturnLink() {
        return returnLink;
    }

    public void setReturnLink() {
        returnLink = driver.findElement(By.linkText("Return"));
    }

    public WebElement getNewLink() {
        return newLink;
    }

    public void setNewLink() {
        newLink = driver.findElement(By.cssSelector("#newbutton"));
    }

    public WebElement getDuplicateButton() {
        return duplicateButton;
    }

    public void setDuplicateButton() {
        duplicateButton = driver.findElement(By.cssSelector("#command-navigation > input:nth-child(2)"));
    }

    public WebElement getEditButton() {
        return editButton;
    }

    public void setEditButton() {
        editButton = driver.findElement(By.cssSelector("#command-navigation > input:nth-child(3)"));
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton() {
        deleteButton = driver.findElement(By.cssSelector("#command-navigation > input:nth-child(4)"));
    }

    public WebElement getActionDropdown() {
        return actionDropdown;
    }

    public void setActionDropdown() {
        actionDropdown = driver.findElement(By.name("cmdSelect"));
    }

    public WebElement getApplyButton() {
        return applyButton;
    }

    public void setApplyButton() {
        applyButton = driver.findElement(By.id("applybutton"));
    }

    public WebElement getCategoryNameDropdown() {
        return categoryNameDropdown;
    }

    public void setCategoryNameDropdown() {
        categoryNameDropdown = driver.findElement(By.name("SelectedCateg"));
    }

    public WebElement getStatusesDropdown() {
        return statusesDropdown;
    }

    public void setStatusesDropdown() {
        statusesDropdown = driver.findElement(By.name("SelectedStatus"));
    }

    public CommentsTable getCommentsTable() {
        return commentsTable;
    }

    public void setCommentsTable() {
        commentsTable = new CommentsTable(driver);
    }

    public WebElement getInfoField() {
        return infoField;
    }

    public void setInfoField() {
        infoField = driver.findElement(By.id("infoField"));
    }

    public Alert getPopup() {
        return popup;
    }

    public void setPopup(final Alert popup) {
        this.popup = popup;
    }

    /* TODO
* select an action
* delete
* */
    // Page usage section

    public EditorPage createNewComment() {
        newLink.click();
        return new EditorPage(driver);
    }

    public IndexPage delete(int... rowPosition) {
        getCommentsTable().uncheckAll();

        for (int i = 1; i <= rowPosition.length; i++) {
            getCommentsTable().checkOffComment(rowPosition[i]);
        }

        deleteButton.click();

        popup = driver.switchTo().alert();

        popup.accept();

        return new IndexPage(driver);
    }

    public void deleteWithNoSelection() {
        getCommentsTable().uncheckAll();

        deleteButton.click();

        popup = driver.switchTo().alert();
    }

    public EditorPage editComment(int rowPosition) {
        getCommentsTable().uncheckAll();
        getCommentsTable().checkOffComment(rowPosition);

        editButton.click();

        return new EditorPage(driver);
    }

    public void editInvalidNumberOfComments(int... rowPosition) {
        getCommentsTable().uncheckAll();

        for (int i = 1; i <= rowPosition.length; i++) {
            getCommentsTable().checkOffComment(rowPosition[i]);
        }

        editButton.click();

        popup = driver.switchTo().alert();
    }

    public EditorPage duplicateComment(int rowPosition) {
        getCommentsTable().uncheckAll();
        getCommentsTable().checkOffComment(rowPosition);

        duplicateButton.click();

        return new EditorPage(driver);
    }

    public void duplicateInvalidNumberOfComments(int... rowPosition) {
        getCommentsTable().uncheckAll();

        for (int i = 1; i <= rowPosition.length; i++) {
            getCommentsTable().checkOffComment(rowPosition[i]);
        }
        duplicateButton.click();

        popup = driver.switchTo().alert();
    }

    public IndexPage filterTable(String categoryName, String statusText) {
        setCategoryFilter(categoryName);
        setStatusFilter(statusText);
        applyButton.click();
        return new IndexPage(driver);
    }

    public void setCategoryFilter (String categoryName) {
        new Select(categoryNameDropdown).selectByVisibleText(categoryName);
    }

    public void setStatusFilter (String statusText) {
        new Select(statusesDropdown).selectByVisibleText(statusText);
    }









    // Inner class section

    public class CommentsTable {
        private WebDriver driver;
        private WebElement numberSortLink;
        private WebElement commentTextSortLink;
        private WebElement activeSortLink;
        private String currentPageNumber;

        public CommentsTable(final WebDriver driver) {
            this.driver = driver;

            numberSortLink = driver.findElement(By.cssSelector(
                    "#main > div > div:nth-child(10) > form > table > thead > tr > th:nth-child(2) > a"));
            commentTextSortLink = driver.findElement(By.cssSelector(
                    "#main > div > div:nth-child(10) > form > table > thead > tr > th:nth-child(3) > a"));
            activeSortLink = driver.findElement(By.cssSelector(
                    "#main > div > div:nth-child(10) > form > table > thead > tr > th:nth-child(4) > a"));
            if (driver.findElements(
                    By.cssSelector("#main > div > div:nth-child(10) > form > table > tfoot"))
                    .size() > 0) {
            setCurrentPageNumber();
            } else {
                currentPageNumber = "0";
            }
            // TODO
        }

        private void setCurrentPageNumber() {
            String fullText = driver.findElement(By.cssSelector(
                    "#main > div > div:nth-child(10) > form > table > tfoot > tr > td")).getText();
            List<WebElement> links = driver.findElements(By.cssSelector(
                    "#main > div > div:nth-child(10) > form > table > tfoot > tr > td > a"));
            for (WebElement link : links) {
                fullText = fullText.replace(link.getText(), "");
            }
            currentPageNumber = fullText.trim();

        }

        public String getCurrentPageNumber() {
            return currentPageNumber;
        }

        public WebElement getCell(final int row, final int column) {
            return driver.findElement(By.cssSelector(
                    "#main > div > div:nth-child(10) > form > table > tbody > tr:nth-child("
                            + row + ") > td:nth-child(" + column + ")"));

        }

        public Comment getComment(int rowPosition) {
            String number = getCell(rowPosition, 2).getText();
            String commentText = getCell(rowPosition, 3).getText();
            boolean activeStatus = getCell(rowPosition, 4)
                    .getText()
                    .contains("V");

            List<String> categories = Arrays.asList(getCell(rowPosition, 5)
                    .getText()
                    .replaceAll("\\s", "")
                    .split(";"));

            return new Comment(number, commentText, activeStatus, categories);

        }

        public IndexPage goToPage(String pageNumber) {
            List<WebElement> availablePages = driver.findElements(By.cssSelector(
                    "#main > div > div:nth-child(10) > form > table > tfoot > tr > td > a"));
            for (WebElement availablePage : availablePages) {
                if (availablePage.getText().trim().equals(pageNumber)) {
                    availablePage.click();
                    return new IndexPage(driver);
                }
            }

            throw new RuntimeException("There is no such page");
        }

        public IndexPage goToNextPage() {
            return goToPage(">");
        }

        public IndexPage  goToPrevPage() {
            return goToPage("<");
        }

        public IndexPage sortByNumber() {
            numberSortLink.click();
            return new IndexPage(driver);
        }

        public IndexPage sortByCommentText() {
            commentTextSortLink.click();
            return new IndexPage(driver);
        }

        public IndexPage sortByActiveStatus() {
            activeSortLink.click();
            return new IndexPage(driver);
        }

        public WebElement checkOffComment(int commentPosition) {
            WebElement checkbox = getCell(commentPosition, 1);
            if (!checkbox.isSelected()) {
                checkbox.click();
            }
            return checkbox;
        }

        public WebElement uncheckComment(int commentPosition) {
            WebElement checkbox = getCell(commentPosition, 1);
            if (checkbox.isSelected()) {
                checkbox.click();
            }
            return checkbox;
        }

        public void checkOffAll() {
            for (int i = 1; i <= 10; i++) {
                checkOffComment(i);
            }
        }

        public void uncheckAll() {
            for (int i = 1; i <= 10; i++) {
                uncheckComment(i);
            }
        }
    }
}
