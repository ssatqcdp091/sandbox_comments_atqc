package com.softserve.edu.zhyliaiev.pages;

import com.softserve.edu.zhyliaiev.comments.Comment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditorPage {
    private WebDriver driver;
    private WebElement returnLink;
    private WebElement refreshLink;
    private WebElement saveButton;
    private WebElement saveAndReturnButton;
    private WebElement commentTextField;
    private WebElement numberField;
    private WebElement activeCheckbox;
    private WebElement allSelectButton;
    private WebElement curSelectButton;
    private WebElement curUnSelectButton;
    private WebElement allUnSelectButton;
    private WebElement generalValidationError;
    private WebElement textValidationError;
    private WebElement numberValidationError;
    private WebElement activeValidationError;
    private CategoriesBox availableCategories;
    private CategoriesBox selectedCategories;

    public EditorPage(final WebDriver driver) {
        setDriver(driver);

        setReturnLink();
        setRefreshLink();
        setSaveButton();
        setSaveAndReturnButton();
        setCommentTextField();
        setNumberField();
        setActiveCheckbox();
        setAllSelectButton();
        setCurSelectButton();
        setCurUnSelectButton();
        setAllUnSelectButton();
        setGeneralValidationError();
        setTextValidationError();
        setNumberValidationError();
        setActiveValidationError();
        setAvailableCategories();
        setSelectedCategories();

    }

    // Getter/Setter section

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(final WebDriver driver) {
        if (driver.getTitle().toLowerCase().startsWith("editor")) {
            this.driver = driver;
        } else {
            throw new RuntimeException("This is not the index page");
        }
    }

    public WebElement getReturnLink() {
        return returnLink;
    }

    public void setReturnLink() {
        returnLink = driver.findElement(By.linkText("Return"));
    }

    public WebElement getRefreshLink() {
        return refreshLink;
    }

    public void setRefreshLink() {
        refreshLink = driver.findElement(By.linkText("Refresh"));
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public void setSaveButton() {
        saveButton = driver.findElement(By.cssSelector("#editor-navigation > input:nth-child(2)"));
    }

    public WebElement getSaveAndReturnButton() {
        return saveAndReturnButton;
    }

    public void setSaveAndReturnButton() {
        saveAndReturnButton = driver.findElement(By.cssSelector("#editor-navigation > input:nth-child(3)"));
    }

    public WebElement getCommentTextField() {
        return commentTextField;
    }

    public void setCommentTextField() {
        commentTextField = driver.findElement(By.id("Text"));
    }

    public WebElement getNumberField() {
        return numberField;
    }

    public void setNumberField() {
        numberField = driver.findElement(By.id("Number"));
    }

    public WebElement getActiveCheckbox() {
        return activeCheckbox;
    }

    public void setActiveCheckbox() {
        activeCheckbox = driver.findElement(By.id("Active"));
    }

    public WebElement getAllSelectButton() {
        return allSelectButton;
    }

    public void setAllSelectButton() {
        allSelectButton = driver.findElement(By.name("AllSelect"));
    }

    public WebElement getCurSelectButton() {
        return curSelectButton;
    }

    public void setCurSelectButton() {
        curSelectButton = driver.findElement(By.name("CurSelect"));
    }

    public WebElement getCurUnSelectButton() {
        return curUnSelectButton;
    }

    public void setCurUnSelectButton() {
        curUnSelectButton = driver.findElement(By.name("CurUnSelectBtn"));
    }

    public WebElement getAllUnSelectButton() {
        return allUnSelectButton;
    }

    public void setAllUnSelectButton() {
        allUnSelectButton = driver.findElement(By.name("AllUnSelectBtn"));
    }


    public WebElement getGeneralValidationError() {
        return generalValidationError;
    }

    public void setGeneralValidationError() {
        generalValidationError = driver.findElement(By.cssSelector("#errorfield"));
    }

    public WebElement getTextValidationError() {
        return textValidationError;
    }

    public void setTextValidationError() {
        textValidationError = driver.findElement(By.cssSelector("span[data-valmsg-for=Text]"));
    }

    public WebElement getNumberValidationError() {
        return numberValidationError;
    }

    public void setNumberValidationError() {
        numberValidationError = driver.findElement(By.cssSelector("span[data-valmsg-for=Number]"));
    }

    public WebElement getActiveValidationError() {
        return activeValidationError;
    }

    public void setActiveValidationError() {
        activeValidationError = driver.findElement(By.cssSelector("span[data-valmsg-for=Active]"));
    }

    public CategoriesBox getAvailableCategories() {
        return availableCategories;
    }

    public void setAvailableCategories() {
        availableCategories = new CategoriesBox(driver, "alvailablecategories");
    }

    public CategoriesBox getSelectedCategories() {
        return selectedCategories;
    }

    public void setSelectedCategories() {
        selectedCategories = new CategoriesBox(driver, "selectedCategories");
    }

    // Page usage section

    public void setCommentData(Comment comment) {
        commentTextField.clear();
        commentTextField.sendKeys(comment.getCommentText());

        numberField.clear();
        numberField.sendKeys(comment.getNumber());

        if (comment.isActive()) {
            if (!activeCheckbox.isSelected()) {
            activeCheckbox.click();
            }
        } else {
            if (activeCheckbox.isSelected()) {
                activeCheckbox.click();
            }
        }

        for (WebElement webElement : availableCategories.getCheckboxes()) {
            WebElement curChecbox = webElement.findElement(By.cssSelector("input[type=checkbox]"));
            WebElement curSpan = webElement.findElement(By.cssSelector("span"));

            if (comment.getCategories().contains(curSpan.getText())) {
                if (!curChecbox.isSelected()) {
                    curChecbox.click();
                }
            }
        }

        for (WebElement webElement : selectedCategories.getCheckboxes()) {
            WebElement curChecbox = webElement.findElement(By.cssSelector("input[type=checkbox]"));
            WebElement curSpan = webElement.findElement(By.cssSelector("span"));

            if (!comment.getCategories().contains(curSpan.getText())) {
                if (!curChecbox.isSelected()) {
                    curChecbox.click();
                }
            }
        }


        curSelectButton.click();
        availableCategories.setCategories();

        curUnSelectButton.click();
        selectedCategories.setCategories();
    }

    public  EditorPage save(Comment comment){
        setCommentData(comment);
        getSaveButton().click();
        return new EditorPage(getDriver());
    }

    public  IndexPage successfulSaveAndReturn(Comment comment) {
        setCommentData(comment);
        getSaveAndReturnButton().click();
        return new IndexPage(getDriver());
    }

    public  EditorPage unsuccessfulSaveAndReturn() {
        getSaveAndReturnButton().click();
        return new EditorPage(getDriver());
    }

    public  EditorPage unsuccessfulSaveAndReturn(Comment comment) {
        setCommentData(comment);
        getSaveAndReturnButton().click();
        return new EditorPage(getDriver());
    }

    public  EditorPage refresh() {
        getRefreshLink().click();
        return new EditorPage(getDriver());
    }

    public IndexPage returnToIndex() {
        getReturnLink().click();
        return new IndexPage(getDriver());
    }



    // Inner Class section

    public class CategoriesBox {
        List<WebElement> checkboxes;
        String id;
        WebElement categoryBox;



        public CategoriesBox(WebDriver driver, String id) {
            setId(id);
            setCategoryBox();
            setCategories();

        }

        public WebElement getCategoryBox() {
            return categoryBox;
        }

        public void setCategoryBox() {
            this.categoryBox = driver.findElement(By.cssSelector("div#" + id));
        }


        public List<WebElement> getCheckboxes() {
            return checkboxes;
        }

        public void setCategories() {
            checkboxes = categoryBox.findElements(
                            By.cssSelector("div.categoryitem"));

        }

        public String getId() {
            return id;
        }

        public void setId(final String id) {
            this.id = id;
        }
    }
}
