package com.softserve.edu.zhyliaiev.comments;

import java.util.List;

public class Comment {
    private String number;
    private String commentText;
    private boolean activeStatus;
    private List<String> categories;


    public Comment(final String number, final String commentText, final boolean activeStatus, final List<String> categories) {
        this.number = number;
        this.commentText = commentText;
        this.activeStatus = activeStatus;
        this.categories = categories;
    }

    public String getNumber() {
        return number;
    }

    public String getCommentText() {
        return commentText;
    }

    public boolean isActive() {
        return activeStatus;
    }

    public List<String> getCategories() {
        return categories;
    }

    @Override
    public String toString() {
        return number + " - \"" + commentText + "\" - " + (activeStatus ? "active - " : "inactive - ") + categories.toString();
    }
}
