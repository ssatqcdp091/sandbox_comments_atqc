package com.softserve.edu.karabaza.test;

import org.testng.annotations.Test;

import com.softserve.edu.karabaza.page.EditorPage;
import com.softserve.edu.karabaza.page.index.IndexPage;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class SomeTest {
	 private WebDriver driver;
	 private String baseURL;
	 
  @Test
  public void clickNextPageTest() {
	  driver.get(baseURL);
	  IndexPage indexPage = new IndexPage(driver);
	  EditorPage editorPage = indexPage.clickNew();
	  editorPage.addAllCategories();
	  editorPage.choosTheSelectedCategory(1);
	  editorPage.choosTheSelectedCategory(3);
	  editorPage.removeCurCategorie();
	  editorPage.fillTheCommText("asdfd");
	  editorPage.fillTheNumber("444");
	  editorPage.checkTheActiveBox();
	  editorPage.clickSaveAndReturn();
  }
  
  @BeforeTest
  public void beforeTest() throws InterruptedException {
	  System.setProperty("webdriver.chrome.driver"
			  , "C:\\Users\\Karabaza1\\webdrivers\\chromedriver.exe");
	  driver = new ChromeDriver();
	  baseURL = "http://commentssprintone.azurewebsites.net";
  }

  @AfterTest
  public void afterTest() {
  }

}
