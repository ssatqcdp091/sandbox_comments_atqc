/**
 * 
 */
package com.softserve.edu.karabaza.page;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.softserve.edu.karabaza.page.index.IndexPage;

/**
 * @author Karabaza Anton
 *
 */
public class EditorPage {
	private WebDriver driver;
	
	private WebElement returnLable;
	
	private WebElement refreshLable;
	private WebElement saveLable;
	private WebElement saveAndReturnLable;
	
	private WebElement commentTextField;
	private WebElement numberField;
	private WebElement activeCheckBox;
	
	private ArrayList<WebElement> availableCategories;
	private ArrayList<WebElement> selectedCategories;
	private WebElement addAllCateg;
	private WebElement addCurCateg;
	private WebElement removeCurCateg;
	private WebElement removeAllCateg;
	
	public EditorPage(WebDriver driver){
		this.driver = driver;
		
		if(!driver.getTitle().toLowerCase().startsWith("editor")){
			throw new RuntimeException("This is not the editor page");
		}
		
		returnLable = driver.findElement(By.linkText("Return"));
		
		refreshLable = driver.findElement(By.linkText("Refresh"));
		saveLable = driver.findElement(By.xpath(
				"//*[@id=\"editor-navigation\"]/input[1]"));
		saveAndReturnLable = driver.findElement(By.xpath(
				"//*[@id=\"editor-navigation\"]/input[2]"));
		
		commentTextField = driver.findElement(By.id("Text"));
		numberField = driver.findElement(By.id("Number"));
		activeCheckBox = driver.findElement(By.id("Active"));
		
		availableCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#alvailablecategories #Categories")));
		selectedCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#selectedCategories #Categories")));	
		addAllCateg = driver.findElement(By.name("AllSelect"));
		addCurCateg = driver.findElement(By.name("CurSelect"));
		removeCurCateg = driver.findElement(By.name("CurUnSelectBtn"));
		removeAllCateg = driver.findElement(By.name("AllUnSelectBtn"));
	}
	
	public IndexPage clickReturn(){
		returnLable.click();
		return new IndexPage(driver);
	}

	public EditorPage clickRefresh(){
		refreshLable.click();
		return new EditorPage(driver);
	}

	public void clickSave(){
		saveLable.click();
	}

	public IndexPage clickSaveAndReturn(){
		saveAndReturnLable.click();
		return new IndexPage(driver);
	}

	public void fillTheCommText(String text){
		commentTextField.click();
		commentTextField.sendKeys(text);
	}
	
	public void fillTheNumber(String number){
		numberField.click();
		numberField.sendKeys(number);
	}

	public void checkTheActiveBox(){
		activeCheckBox.click();
	}

	public void choosTheUnselectedCategory(int categ){
		availableCategories.get(categ-1).click();
	}

	public void choosTheSelectedCategory(int categ){
		selectedCategories.get(categ-1).click();
	}
	
	public void addAllCategories(){
		addAllCateg.click();
		availableCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#alvailablecategories #Categories")));
		selectedCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#selectedCategories #Categories")));
	}

	public void addCurCategorie(){
		addCurCateg.click();
		availableCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#alvailablecategories #Categories")));
		selectedCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#selectedCategories #Categories")));
	}

	public void removeCurCategorie(){
		removeCurCateg.click();
		availableCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#alvailablecategories #Categories")));
		selectedCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#selectedCategories #Categories")));
	}
	
	public void removeAllCategories(){
		removeAllCateg.click();
		availableCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#alvailablecategories #Categories")));
		selectedCategories = new ArrayList<WebElement>(
				driver.findElements(By.cssSelector("#selectedCategories #Categories")));
	}
}
