/**
 * 
 */
package com.softserve.edu.karabaza.page.index;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author Karabaza1
 *
 */
public class CommentsTable {
	private WebDriver driver;
	
	private WebElement numberLable;
	private WebElement commentTextLable;
	private WebElement inactiveLable;
	
	private ArrayList<WebElement> checkBoxes;
	
	private WebElement previousPage;
	private WebElement nextPage;
	private WebElement somePage;
	
	public CommentsTable(WebDriver driver){
		this.driver = driver;
		
		numberLable = driver.findElement(By.linkText("Number"));
		commentTextLable = driver.findElement(By.linkText("Comment Text"));
		inactiveLable = driver.findElement(By.linkText("Inactive"));
		
		checkBoxes = new ArrayList<WebElement>(driver.findElements(
				By.name("SelectedId")));
	}

	public IndexPage clickNextPage(){
		nextPage = driver.findElement(By.linkText(">"));
		nextPage.click();
		return new IndexPage(driver);
	}
	
	public IndexPage clickPrevPage(){
		previousPage = driver.findElement(By.linkText("<"));
		previousPage.click();
		return new IndexPage(driver);
	}
	
	public IndexPage clickSomePage(int pageToGo){
		String page = Integer.toString(pageToGo);
		somePage = driver.findElement(By.linkText(page));
		somePage.click();
		return new IndexPage(driver);
	}
	
	public IndexPage sortByNumber(){
		numberLable.click();
		return new IndexPage(driver);
	}
	
	public IndexPage sortByCommentText(){
		commentTextLable.click();
		return new IndexPage(driver);
	}
	
	public IndexPage sortByInactive(){
		inactiveLable.click();
		return new IndexPage(driver);
	}
	
	public void checkTheBox(int boxNum){
		checkBoxes.get(boxNum-1).click();
	}
}
