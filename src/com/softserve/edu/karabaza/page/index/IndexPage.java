package com.softserve.edu.karabaza.page.index;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.softserve.edu.karabaza.page.EditorPage;

/**
 * 
 * @author Karabaza Anton
 *
 */
public class IndexPage {
	private WebDriver driver;
	
	private WebElement returnLable;
	
	private WebElement newLable;
	private WebElement duplicateLable;
	private WebElement editLable;
	private WebElement deleteLable;
	private Select selectAnAction;
	
	private Select categoryNameSelect;
	private Select statusesSelect;
	private WebElement applyButton;
	
	private CommentsTable table;
	
	public IndexPage(WebDriver driver){
		this.driver = driver;
		
		if(!driver.getTitle().toLowerCase().startsWith("index")){
			throw new RuntimeException("This is not the index page");
		}
		
		returnLable = driver.findElement(By.linkText("Return"));
		
		newLable = driver.findElement(By.id("newbutton"));
		duplicateLable = driver.findElement(By.xpath(
				"//*[@id=\"command-navigation\"]/input[1]"));
		editLable = driver.findElement(By.xpath(
				"//*[@id=\"command-navigation\"]/input[2]"));
		deleteLable = driver.findElement(By.xpath(
				"//*[@id=\"command-navigation\"]/input[3]"));
		selectAnAction = new Select(driver.findElement(By.id("commandSelect")));
		
		categoryNameSelect = new Select(driver.findElement(By.id("SelectedCateg")));
		statusesSelect = new Select(driver.findElement(By.id("SelectedStatus")));
		applyButton = driver.findElement(By.id("applybutton"));
		
		table = new CommentsTable(driver);
	}
	
	public CommentsTable getTable(){
		return table;
	}

	public IndexPage clickReturn(){
		returnLable.click();
		return new IndexPage(driver);
	}

	public IndexPage selectAnAction(String action){
		selectAnAction.selectByValue(action);
		return new IndexPage(driver);
	}
	
	public IndexPage selectCategoryName(String category){
		categoryNameSelect.selectByVisibleText(category);
		return new IndexPage(driver);
	}
	
	public IndexPage selectStatus(String status){
		statusesSelect.selectByValue(status);
		return new IndexPage(driver);
	}
	
	public IndexPage clickApply(){
		applyButton.click();
		return new IndexPage(driver);
	}
	
	public IndexPage clickDelete(){
		deleteLable.click();
		return new IndexPage(driver);
	}
	
	public EditorPage clickNew(){
		newLable.click();
		return new EditorPage(driver);
	}
	
	public EditorPage clickDuplicate(){
		duplicateLable.click();
		return new EditorPage(driver);
	}

	public EditorPage clickEdit(){
		editLable.click();
		return new EditorPage(driver);
	}
}
