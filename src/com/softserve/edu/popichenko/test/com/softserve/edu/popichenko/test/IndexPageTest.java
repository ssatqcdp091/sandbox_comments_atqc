package com.softserve.edu.popichenko.test.com.softserve.edu.popichenko.test;

import com.softserve.edu.popichenko.pages.AbstractPage;
import com.softserve.edu.popichenko.pages.index.IndexPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Eugene_P on 24-Jan-16.
 */
public class IndexPageTest extends AbstractPageTest {

    @Test
    public void dummy() {
        indexPage.clickNewButton();
    }
}