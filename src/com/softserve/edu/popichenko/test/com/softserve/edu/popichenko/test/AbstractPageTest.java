package com.softserve.edu.popichenko.test.com.softserve.edu.popichenko.test;

import com.softserve.edu.popichenko.pages.AbstractPage;
import com.softserve.edu.popichenko.pages.index.IndexPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

/**
 * Created by Eugene_P on 26-Jan-16.
 */
public class AbstractPageTest {

    protected WebDriver driver;
    protected IndexPage indexPage;

    @BeforeMethod
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        indexPage = new AbstractPage(driver).navigateToIndexPage();

    }

    @AfterMethod
    public void tearDown() throws Exception {
        try {
            driver.quit();
            System.out.println("fin");
        } catch (Exception e) {
            // ignore
        }
    }
}
