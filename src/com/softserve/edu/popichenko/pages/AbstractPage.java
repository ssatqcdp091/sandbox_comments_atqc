package com.softserve.edu.popichenko.pages;

import com.softserve.edu.popichenko.pages.index.IndexPage;
import com.softserve.edu.popichenko.pages.newcoment.NewCommentPage;
import org.openqa.selenium.WebDriver;

/**
 * Created by Eugene_P on 26-Jan-16.
 */
public class AbstractPage {
    private WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    public IndexPage navigateToWebApp() {
        driver.get("http://comments.azurewebsites.net/");
        return new IndexPage(driver);
    }

    public IndexPage navigateToIndexPage() {
        driver.get("http://comments.azurewebsites.net/");
        return new IndexPage(driver);
    }

    public NewCommentPage navigateToNewCommentPage() {
        driver.get("http://comments.azurewebsites.net/Editor/NewComment");
        return new NewCommentPage(driver);
    }

    public WebDriver getDriver() {
        return driver;
    }
}
// just anothe string from IDEA
