package com.softserve.edu.popichenko.pages.index;

import com.softserve.edu.popichenko.pages.AbstractPage;
import com.softserve.edu.popichenko.pages.newcoment.NewCommentPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by Eugene_P on 24-Jan-16.
 */
public class IndexPage extends AbstractPage {


    // header
    @FindBy(id = "title")
    private WebElement title;

    @FindBy(css = "#logindisplay>a")
    private WebElement refresh;
    // div id="command-navigation"
    @FindBy(xpath = ".//*[@id='command-navigation']/input[1]")
    private WebElement newButton;
    @FindBy(xpath = ".//*[@id='command-navigation']/input[2]")
    private WebElement duplicateButton;
    @FindBy(xpath = ".//*[@id='command-navigation']/input[3]")
    private WebElement editButton;
    @FindBy(xpath = ".//*[@id='command-navigation']/input[4]")
    private WebElement deleteButton;
    @FindBys({@FindBy(id = "commandSelect")})
    private List<WebElement> cmdSelect;
    // div id="filters"
    @FindBys({@FindBy(id = "SelectedCateg")})
    private List<WebElement> selectedCateg;
    @FindBys({@FindBy(id = "SelectedStatus")})
    private List<WebElement> selectedStatus;
    @FindBy(id = "applybutton")
    private WebElement applySubmitBtn;
    // table class="webgrid"
    // thead
    // tr class="webgrid-header"
    @FindBy(xpath = ".//*[@id='main']/div/div[5]/form/table/thead/tr/th[2]/a")
    private WebElement numberSortBtn;
    @FindBy(xpath = ".//*[@id='main']/div/div[5]/form/table/thead/tr/th[3]/a")
    private WebElement commentTextSortBtn;
    @FindBy(xpath = ".//*[@id='main']/div/div[5]/form/table/thead/tr/th[4]/a")
    private WebElement activeSortBtn;
    // tbody
    @FindBy(xpath = ".//*[@id='main']/div/div[5]/form/table/tbody")
    // TODO: check if this string is needed... as far as separate PageFactory.initElements() is called in constructor
    private TableBody tbody; // TODO: inited with PageFactory somehow..., not sure if it is correct

    // tfoot
    // tr class="webgrid-footer"
    @FindBys({
            @FindBy(xpath = ".//*[@id='main']/div/div[5]/form/table/tfoot/tr/td")
    })
    private List<WebElement> paging;


    public IndexPage(WebDriver driver) {
        super(driver);
        // Here can be some validation like: if we got the page we wanted
        if (!driver.getTitle().equals("Index")) {
            throw new RuntimeException("This is not the login page");
        }

        //This initElements method will create all WebElements
        // sets the field value only after we call it
        // @CacheLookup - puts element to cache. If we don’t do it, then every time when we turn to our element, WebDriver will check if the element is present on the page.
        PageFactory.initElements(driver, this);
//        tbody = PageFactory.initElements(driver, TableBody.class); // TODO: inited with PageFactory somehow..., not sure if it is correct
    }

    public NewCommentPage clickNewButton() {
        newButton.click();
        return navigateToNewCommentPage();
    }

}
