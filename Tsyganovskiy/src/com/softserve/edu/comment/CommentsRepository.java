package com.softserve.edu.comment;

public class CommentsRepository {

	public static Comment ValidComment() {
		return new Comment().setCommentText("TestComment").setCommentNumber(457);
	}

	public static Comment inValidCommentTextFieldEmpty() {
		return new Comment().setCommentText("").setCommentNumber(457);
	}

	public static Comment inValidCommentNumberMore999() {
		return new Comment().setCommentText("TestComment").setCommentNumber(1001);
	}

}
