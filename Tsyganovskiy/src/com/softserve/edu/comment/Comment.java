package com.softserve.edu.comment;

public class Comment {
	private String commentText;
	private int commentNumber;

	public String getCommentText() {
		return commentText;
	}

	public int getCommentNumber() {
		return commentNumber;
	}

	public Comment setCommentText(String commentText) {
		this.commentText = commentText;
		return this;
	}

	public Comment setCommentNumber(int commentNumber) {
		this.commentNumber = commentNumber;
		return this;
	}

}
