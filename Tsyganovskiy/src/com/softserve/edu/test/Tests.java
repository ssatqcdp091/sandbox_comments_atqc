package com.softserve.edu.test;

import com.softserve.edu.comment.CommentsRepository;
import com.softserve.edu.pages.IndexPage;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Tests {

	private WebDriver driver;

	@BeforeClass
	public final void setUp() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://commentssprintone.azurewebsites.net");
	}

	/**
	 * Test link "New".
	 */
	@Test
	public final void clickNewLinkTest() {
		IndexPage indexpage = new IndexPage(driver);
		indexpage.clickNewLink();
		indexpage.clickReturn();
	}

	/**
	 * Test link "Edit".
	 */
	@Test
	public final void clickEditLinkTest() {
		IndexPage indexpage = new IndexPage(driver);
		indexpage.clickEditLink();
		indexpage.clickReturn();
	}

	/**
	 * Test delete function with confirmation.
	 */
	@Test
	public final void clickDeleteLinkConfirmationTest() {
		IndexPage indexpage = new IndexPage(driver);
		WebElement comment = driver.findElement(By.xpath("//tbody/tr"));
		comment.findElement(By.xpath(".//input[@type='checkbox']")).click();
		indexpage.clickDeleteLink();
		indexpage.DeleteConfirmationDialog();
	}

	/**
	 * Test delete function without confirmation.
	 */
	@Test
	public final void clickDeleteLinkNotConfirmationTest() {
		IndexPage indexpage = new IndexPage(driver);
		WebElement comment = driver.findElement(By.xpath("//tbody/tr"));
		comment.findElement(By.xpath(".//input[@type='checkbox']")).click();
		indexpage.clickDeleteLink();
		indexpage.DeleteNotConfirmationDialog();
	}

	/**
	 * Test delete function without checked comments.
	 */
	// @Test
	// public final void clickDeleteLinkTest() {
	// IndexPage indexpage = new IndexPage(driver);
	// indexpage.clickDeleteLink();
	// try {
	// // Check the presence of alert
	// Alert alert = driver.switchTo( ).alert( );
	// // if present consume the alert
	// alert.accept( );
	// } catch (NoAlertPresentException ex) {
	// // Alert not present
	// ex.printStackTrace( );
	// }
	// }

	/**
	 * Test of checkComments method.
	 */
	@Test
	public final void checkCommentsTest() {
		IndexPage indexpage = new IndexPage(driver);
		indexpage.checkComments(5);
	}

	/**
	 * Test of setTablePage method.
	 */
	@Test
	public final void setTablePageTest() {
		IndexPage indexpage = new IndexPage(driver);
		indexpage.setTablePage(3);
	}

	/**
	 * Test of pressApplyButton method.
	 */
	@Test
	public final void pressApplyButtonTest() {
		IndexPage indexpage = new IndexPage(driver);
		indexpage.pressApplyButton();
	}

	/**
	 * WebDriver quit.
	 */
	@AfterClass
	public void tearDown() throws Exception {
		try {
			driver.close();
		} catch (Exception e) {
			// Ignore errors if unable to close the browser
		}
	}
}
