package com.softserve.edu.pages;

import java.util.List;
import java.util.NoSuchElementException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class IndexPage {

	private WebDriver driver;

	public IndexPage(WebDriver driver) {
		this.driver = driver;
		// Check Page Exist
		if (!driver.getTitle().startsWith("Index")) {
			throw new RuntimeException("This is not the Index page");
		}
	}

	public void clickNewLink() {
		driver.findElement(By.xpath(".//*[@id='newbutton']")).click();
	}

	public void clickEditLink() {
		driver.findElement(By.xpath("//input[@value='Duplicate...']")).click();
	}

	public void clickDuplicateLink(WebDriver driver) {
		driver.findElement(By.xpath("//input[@value='Edit..']")).click();
	}

	public void clickDeleteLink() {
		WebElement element = driver.findElement(By.xpath("//input[@value='Delete']"));
		element.click();
	}

	public void clickReturn() {
		driver.findElement(By.linkText("Return")).click();
	}

	public void DeleteConfirmationDialog() {
		driver.findElement(By.xpath("//button[contains(.,'Yes')]")).click();
	}

	public void DeleteNotConfirmationDialog() {
		driver.findElement(By.xpath("//button[contains(.,'No')]")).click();
	}

	public void setSelectAnAction(String str) {
		Select select = new Select(driver.findElement(By.id("commandSelect")));
		select.selectByValue(str);
	}

	public void setCategoryname(String str) {
		Select select = new Select(driver.findElement(By.id("SelectedCateg")));
		select.selectByValue(str);
	}

	public void setStatuses(String str) {
		Select select = new Select(driver.findElement(By.id("SelectedStatus")));
		select.selectByValue(str);
	}

	public void pressApplyButton() {
		driver.findElement(By.id("applybutton")).submit();
	}

	public void clickNumberLink() {
		driver.findElement(By.linkText("Number")).click();
	}

	public void clickCommentTextLink() {
		driver.findElement(By.linkText("Comment Text")).click();
	}

	public void clickInactiveLink() {
		driver.findElement(By.linkText("Inactive")).click();
	}

	public void checkComments(int number) {
		List<WebElement> commentsList = driver.findElements(By.xpath("//tbody/tr"));
		for (int i = 0; i <= number; i++) {
			commentsList.get(i).findElement(By.xpath(".//input[@type='checkbox']")).click();
		}
	}

	public void setTablePage(int pageNumber) {
		try {
			if (pageNumber < 0) {
				System.out.println("Error! Check input value. It must be POSITIVE integer");
			}
			driver.findElement(By.linkText(Integer.toString(pageNumber))).click();
		} catch (NoSuchElementException e) {
			System.out.println("Error! There is no such page. Check input value");
		} 
	}
}
