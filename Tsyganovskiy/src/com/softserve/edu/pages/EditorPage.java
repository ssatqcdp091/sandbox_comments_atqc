package com.softserve.edu.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditorPage {
	private WebDriver driver;

	public EditorPage(WebDriver driver) {
		this.driver = driver;
		// Check Page Exist
		if (!driver.getTitle().startsWith("Editor")) {
			throw new RuntimeException("This is not the Index page");
		}
	}

	public void clickRefresh() {
		driver.findElement(By.linkText("Refresh")).click();
	}

	public void clickSave() {
		driver.findElement(By.xpath("//input[@value='Save']"));
	}

	public void clickSaveReturn() {
		driver.findElement(By.xpath("//input[@value='Save & Return']"));
	}

	public void typeCommentTextField(String str) {
		driver.findElement(By.id("Text")).sendKeys(str);
	}

	public void typeNumberField(String str) {
		driver.findElement(By.id("Number")).sendKeys(str);
	}

	public void setActiveCheckBox() {
		driver.findElement(By.id("Active")).click();
	}

	public void checkCategories(int number) {
		List<WebElement> categoriesList = driver.findElements(By.className("categoryitem"));
		for (int i = 0; i <= number; i++) {
			categoriesList.get(i).findElement(By.xpath(".//input[@type='checkbox']")).click();
		}
	}

	public void moveSelectedCategories() {
		driver.findElement(By.xpath("//input[@value='>']")).click();
	}

	public void moveBackSelectedCategories() {
		driver.findElement(By.xpath("//input[@value='<']")).click();
	}

	public void moveAllCategories() {
		driver.findElement(By.xpath("//input[@value='>>']")).click();
	}

	public void moveBackAllCategories() {
		driver.findElement(By.xpath("//input[@value='<<']")).click();
	}

	public WebElement presenceErrorMessage() {
		return new WebDriverWait(driver, 10).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(ErrorMessages.MSG1_LOCATOR)));
	}
}
